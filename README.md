# README #

Telstra challenge IOT bin and ELEC3607 final project. Uses ESP8266 12E and Arduino Due, as well as python flask server.

### How do I get set up? ###

Uses PlatformIO to compile Arduino style code for ESP8266. Get platformIO and compile and upload ESP8255 server code to an ESP8266. Due bin code can be uploaded using same method.
To get flask server up and running install flask on your machine and run main.py in main folder. 

### Who do I talk to? ###

* Joshua A Manogaran, josh.14668@gmail.com